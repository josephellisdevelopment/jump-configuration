<?php

namespace Jump\Configuration\Ini;

use Jump\Configuration\BaseConfiguration;

class IniConfiguration extends BaseConfiguration
{
    public function __construct(string $filePath) {
        parent::__construct();
        $this->config = parse_ini_file($filePath, true);
    }
}
