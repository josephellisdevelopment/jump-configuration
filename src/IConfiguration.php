<?php

namespace Jump\Configuration;

use Jump\Configuration\Exceptions\InvalidConfigKeyException;

interface IConfiguration
{
    /**
     * Returns a config value as an integer
     * @param string $key
     * @return int
     * @throws InvalidConfigKeyException
     */
    public function getIntValue(string $key): int;

    /**
     * Returns a config value as a string
     * @param string $key
     * @return string
     * @throws InvalidConfigKeyException
     */
    public function getStringValue(string $key): string;

    /**
     * Returns a config value
     * @param string $key
     * @return mixed
     * @throws InvalidConfigKeyException
     */
    public function getValue(string $key);
}
