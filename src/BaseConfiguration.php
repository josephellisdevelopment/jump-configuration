<?php

namespace Jump\Configuration;

use Jump\Configuration\Exceptions\InvalidConfigKeyException;

class BaseConfiguration implements IConfiguration
{
    protected array $config;

    public function __construct() {
        $this->config = [];
    }

    /**
     * Returns a config value as an integer
     * @param string $key
     * @return int
     * @throws InvalidConfigKeyException
     */
    public function getIntValue(string $key): int
    {
        return intval($this->getValue($key));
    }

    /**
     * Returns a config value as a string
     * @param string $key
     * @return string
     * @throws InvalidConfigKeyException
     */
    public function getStringValue(string $key): string
    {
        return strval($this->getValue($key));
    }

    /**
     * Returns a config value
     * @param string $key
     * @return mixed
     * @throws InvalidConfigKeyException
     */
    public function getValue(string $key)
    {
        return $this->getRawValueFromKey($this->config, $this->getKeyArray($key));
    }

    /**
     * Builds an array from a key that parses a config structure, using : as the separator.
     * @param string $key
     * @return array
     */
    private function getKeyArray(string $key): array {
        return explode(":", $key);
    }

    /**
     * Recursive function that gets a value from the config structure using an array of keys, with an element
     * representing each level of the structure.
     * @param array $src
     * @param array $keys
     * @return mixed
     * @throws InvalidConfigKeyException
     */
    private function getRawValueFromKey(array $src, array $keys) {
        if (count($keys) == 1) {
            return $this->config[$keys[0]];
        }
        else if (count($keys) > 1) {
            $newSrc = $src[$keys[0]];
            $keys = array_shift($keys);
            return $this->getRawValueFromKey($newSrc, $keys);
        }
        else {
            throw new InvalidConfigKeyException("No config key was specified.");
        }
    }
}
