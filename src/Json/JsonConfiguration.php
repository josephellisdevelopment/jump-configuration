<?php

namespace Jump\Configuration\Json;

use Jump\Configuration\BaseConfiguration;

class JsonConfiguration extends BaseConfiguration
{
    public function __construct(string $filePath) {
        parent::__construct();
        $contents = file_get_contents($filePath);
        $this->config = json_decode($contents, true);
    }
}
