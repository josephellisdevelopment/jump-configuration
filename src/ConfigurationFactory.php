<?php

namespace Jump\Configuration;

use Jump\Configuration\Json\JsonConfiguration;

class ConfigurationFactory
{
    public function getConfigurationFromJsonFile(string $filePath): IConfiguration {
        return new JsonConfiguration($filePath);
    }
}
